extends Node2D

export var ACTIVE = false

var lookup = {KEY_W: "UP", KEY_S: "DOWN", KEY_A: "LEFT", KEY_D: "RIGHT"}

func _input(event):
	if event is InputEventKey && event.pressed && !ACTIVE:
		var ev = InputEventAction.new()
		var action = lookup.get(event.scancode)
		if action != null:
			ev.action = action
			Input.parse_input_event(ev)

func _ready():
	var name = OS.get_name()
	if name == "Android" || name == "iOS":
		ACTIVE = true
		$UP.visible = true
		$DOWN.visible = true
		$LEFT.visible = true
		$RIGHT.visible = true
