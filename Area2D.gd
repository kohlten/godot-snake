extends Area2D

func _input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.is_pressed():
		if get_parent().get_parent().ACTIVE:
			print("PRESSED " + get_parent().name)
			var ev = InputEventAction.new()
			ev.action = get_parent().name
			Input.parse_input_event(ev)
