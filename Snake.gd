extends Node2D

signal timer_end

var Tail = preload("res://Tail.tscn")
export var SIZE = 1
export var TAIL = []
export var PAUSED = false
export var DIED = false

enum SnakeDir {UP, DOWN, LEFT, RIGHT, NONE}

var DIR = SnakeDir.NONE
var PIXEL_SIZE = 40
var TIME = 0

const windowSize = Vector2(1280, 720)

var deathTimer = Timer.new()

func new_tail(pos):
	deathTimer.set_wait_time(1.0)
	deathTimer.connect("timeout", self, "_deathTimer_Timeout")
	add_child(deathTimer)
	var tail = Tail.instance()
	tail.position = pos
	TAIL.append(tail)
	add_child(tail)

func _deathTimer_Timeout():
	reset()
	get_tree().change_scene("res://menu.tscn")

func _add_tail_size():
	SIZE += 1

func _input(event):
	if event is InputEventAction:
		if event.action == "UP":
			DIR = SnakeDir.UP
		elif event.action == "DOWN":
			DIR = SnakeDir.DOWN
		elif event.action == "LEFT":
			DIR = SnakeDir.LEFT
		elif event.action == "RIGHT":
			DIR = SnakeDir.RIGHT

func reset():
	DIR = SnakeDir.NONE
	$body.position = Vector2(0, 0)
	for node in TAIL:
		node.queue_free()
	TAIL = []
	SIZE = 1
	PAUSED = false
	DIED = false

func hitTailOrSize():
	if $body.position.x < 0 || $body.position.y < 0 || \
		$body.position.x + PIXEL_SIZE >= windowSize.x || \
		$body.position.y + PIXEL_SIZE >= windowSize.y:
			return true
	for node in TAIL:
		if node.position == $body.position:
			return true
	return false

func processTail():
	if DIR != SnakeDir.NONE && len(TAIL) < SIZE:
		new_tail($body.position)
	if len(TAIL) >= SIZE && len(TAIL) > 0:
		var node = TAIL.pop_front()
		node.queue_free()

func processDir():
	match DIR:
		SnakeDir.UP:
			$body.position.y -= PIXEL_SIZE
		SnakeDir.DOWN:
			$body.position.y += PIXEL_SIZE
		SnakeDir.LEFT:
			$body.position.x -= PIXEL_SIZE
		SnakeDir.RIGHT:
			$body.position.x += PIXEL_SIZE

func _process(delta):
	TIME += delta
	if hitTailOrSize() && deathTimer.is_stopped():
		DIED = true
		deathTimer.start()
	if TIME >= 0.12 and not PAUSED and not DIED:
		TIME = 0
		processTail()
		processDir()
