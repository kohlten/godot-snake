extends Node2D

var width = 1280
var height = 720

export var SNAKE_SIZE = 40

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	set_food_pos()

func set_food_pos():
	var found = false
	var pos = Vector2()
	while !found:
		pos.x = (randi() % (width / SNAKE_SIZE)) * SNAKE_SIZE
		pos.y = (randi() % (height / SNAKE_SIZE)) * SNAKE_SIZE
		var tail = get_node("Snake").TAIL
		var body = get_node("Snake/body")
		if len(tail) == 0 and (body.position.x != pos.x || body.position.y != pos.y):
			found = true
		for node in tail:
			if body.position.x != pos.x || body.position.y != pos.y:
				found = true
				break
		var controllerPos = Vector2(970, 405)
		var controllerSize = Vector2(314, 316)
		var rect = Rect2(controllerPos, controllerSize)
		if rect.has_point(pos):
			found = false
	$Food/KinematicBody2D.position = pos

func _process(delta):
	if $Snake/body.position == $Food/KinematicBody2D.position:
		get_node("Snake")._add_tail_size()
		set_food_pos()
	$Fps.text = String(Engine.get_frames_per_second())
	if int($Size.text) != get_node("Snake").SIZE:
		$Size.text = String(get_node("Snake").SIZE)
	if $Snake.DIED and $DeathLabel.visible == false:
		$DeathLabel.visible = true
	elif !$Snake.DIED and $DeathLabel.visible == true:
		$DeathLabel.visible = false
